#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 12:46:35 2021

@author: lucas
"""
from scipy.interpolate import RegularGridInterpolator as rgi
import sys, os
import numpy as np

def gridInterpolator(X, Y, Z, inPhase, gridVals):
    
    if len(gridVals) != 3:
        print("Error: gridVals must be list of length 3 containing X, Y and Z coordinates")
        print("------------------------------------------")
        sys.exit()
    #reading LaMEM grid values
    LX = gridVals[0]
    LY = gridVals[1]
    LZ = gridVals[2]    
    #create 3d array of the point where we want to interpolate
    pts = np.array([LX.flatten(),LY.flatten(),LZ.flatten()])
    
    #transpose 
    pts = np.transpose(pts)
    #create interpolation function
    interp3 = rgi((X, Y, Z), inPhase, method="linear")
    try:
        Phase = interp3(pts)

    except:

        print("---------ERORR----------------------------")
        print("------------------------------------------")
        print("An error while interpolating occured. This is most likely due to the LaMEM grid")
        print("being larger or same size as your initial phase grid")
        print("------------------------------------------")
        sys.exit()
    Phase = np.rint(Phase)
    Phase = np.reshape(Phase,(LX.shape))
    
    return Phase