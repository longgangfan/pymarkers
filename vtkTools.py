#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 29 19:16:28 2020

@author: lucas
"""

import os,sys
import numpy as np
from vtk import vtkXMLPRectilinearGridReader, vtkXMLPStructuredGridReader
from vtk.util import numpy_support as VN
import matplotlib.pyplot as plt
from uvw import RectilinearGrid, DataArray
#import ipdb

def writeVTK(lamemGrid):
    Phase = lamemGrid.Phase
    x = lamemGrid.x
    y = lamemGrid.y
    z = lamemGrid.z
    print("------------------------")
    print("Writing vtk file to output/BlockPhase.vtr")
    print("------------------------")
    grid = RectilinearGrid('output/BlockPhase.vtr', (x, y, z), compression=True)
    grid.addPointData(DataArray(Phase, range(3), 'Phase'))
    grid.write()
    
    return

def SaveData(nel, i, fieldname = 'velocity [cm/yr]'):
    res = np.array(nel)
    res = res +1
    ele_z = res[2]-1
    diff_x = np.array([])
    diff_z = np.array([])
    vel_x = np.array([])
    vel_z = np.array([])
    counter = 0
    for subdir, dirs, files in os.walk(os.getcwd()):
        for folders in dirs:
            
            if "Timestep" in folders:
                counter = counter +1
                #if counter ==2:
                    #sys.exit()
               # print(os.path.join(subdir,folders))
                os.chdir(os.path.join(subdir, folders))
    
                fname = 'Block_surf.pvts'
                #print("checkmark")
                
                reader = vtkXMLPStructuredGridReader()
                reader.SetFileName(fname)
                reader.Update()
                
                data = reader.GetOutput()
                 
                #get velocity at z elevation 
                
                
                
                # fieldname = 'j2_strain_rate [1/s]'
                # fieldname = 'phase [ ]'
                # fieldname = 'velocity [cm/yr]'
                
                field = VN.vtk_to_numpy(data.GetPointData().GetArray(fieldname))
                #x =  len(field)**(1/3)
                v_z = field[:,2]
                #v_x = field[:,0]
                # dummy= np.empty([])
                # for i in range(len(field)):
                #     if field[i].astype(int)-field[i]==0:
                        
                #         #print("hello")
                #         dummy = np.append([i], dummy)
                #x=int(x)
                #nx,ny,nz = 
                #field = np.reshape(field, (, order = 'F')
                #field = field[:,0]
                #v_x = v_x.reshape((res),order='F').swapaxes(-1,0)
                v_z = v_z.reshape((res[1],res[2]),order='F').swapaxes(-1,0)
                
                #v_z = v_z[ele_z,1,:]
                #v_z = abs(v_z)
                #v_x = v_x[ele_z,1,:]
                #v_x = abs(v_x)
                #x = np.linspace(0,400,401)
                #for i in range(len(v_x)):
                #vel_x = np.append([vel_x],v_x[:])#, axis = counter)
                vel_z = np.append([vel_z],v_z[:,0])#, axis = counter)
                #ipdb.set_trace()
                # plt.figure(counter)
                # plt.plot(x,v_z,'o')
                # #plt.plot(x,v_x,'o')
                # # #fig.scatter(field)
                
                # plt.show()
                #ipdb.set_trace()
    #vel_x = vel_x.reshape((res[0], counter))
    vel_z = vel_z.reshape((res[0], counter), order = "F")
    #x = np.linspace(0,400,401)
    os.chdir("..")
    #wd = os.getcwd
    file = "velocity" + str(i)
    
    
    np.save(str(file), vel_z)
    return vel_z